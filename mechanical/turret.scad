include <lib/bearing.scad>
include <lib/involute_gears.scad> 
include <lib/roundedcube.scad>

// Mounting bracket (ceiling)
mounting_bracket_diam = 106.12;

// Controls how much extra material is on top plates
turret_top_padding = 10;

// Mounting posts
post_diam = 10;
post_screw_diam = 2;

// Shroud
shroud_height = 200;

// 3D Print settings
print_wall_thickness = 5;

// Hexagonal air holes
hexagon_scale = 3;
hexagon_padding = 1;    // mm between hexagons
hexagon_rotation = 15;  // deg rotation
x_offset_hexagons = 1;  // x deflection (twist)
y_offset_hexagons = 1;  // y deflection (spiral)

// Dimensions
turret_rotational_base_diameter = mounting_bracket_diam - turret_top_padding;

// Drive gear
turret_rotational_drive_gear_diameter = 15.6;
turret_rotational_drive_gear_teeth = 17;

// Nerf gun mounting arm parameters
cone_hgt = 25;
cone_wall_thickness = 5;
cone_upper_radius = 15;
cone_wire_passthrough_radius = 5;
// Angular thickness of connecting arms at point of joining cone
cone_arm_base_angle = 25;
arm_separation = 60;
arm_height = 50;
vertical_arm_height = 105;
vertical_arm_x_displacement = 32.5;
vertical_arm_w = 5;
vertical_arm_h = 20;

// Nerf gun pivot bar placement
pivot_rod_diameter = 6;
nerf_gun_pivot_pt_y = 360;
nerf_gun_pivot_pt_z = 130;

// Tolerances for geometry connections.
AT=0.02;
ST=AT*2;
TT=AT/2;
FN=100;     // Number of polygons in a cylinder, set higher for final render to laser cut
PI=3.14159265359;

// Calculate dimensions used
driven_shaft_position_r = turret_rotational_base_diameter/2+turret_rotational_drive_gear_diameter/2-3.5;
turret_top_d = turret_rotational_base_diameter + pow(turret_rotational_base_diameter, 0.5) * 2 + turret_top_padding;

// Radius position of column supports
column_r = turret_top_d / 2 - post_diam/2;

// Functions
module connecting_arm(){
    rotate_extrude(angle=cone_arm_base_angle, $fn=FN) polygon( points=[
    // Define rotational extrusion polygon
        [cone_wire_passthrough_radius, 0],
        [arm_separation / 2, arm_height],
        [arm_separation / 2 + cone_wall_thickness, arm_height],
        [cone_upper_radius, 0]
    ]);
}

module hull_arm(include_bearing = true){
    bearing = 606;
    bearing_width = bearingWidth(bearing);
    bearing_outer_diam = bearingOuterDiameter(bearing);
    bearing_surround_walls = 10;
    difference(){
        union(){
            hull(){
                translate([0,0,cone_hgt + arm_height]){
                    linear_extrude(height=0.1){
                        projection(cut=true){
                            translate([0,0, -arm_height]){
                                connecting_arm();
                            }
                        }
                    }
                }
                translate([vertical_arm_x_displacement,0,cone_hgt + arm_height + vertical_arm_height]){
                    linear_extrude(height=1){
                        square(size=[vertical_arm_w, vertical_arm_h], center=true);
                    }
                }
            }
            translate([vertical_arm_x_displacement,0, 40 + cone_hgt + vertical_arm_height]){
                rotate([0, 90, 0]){
                    cylinder(bearing_width, d=bearing_outer_diam + bearing_surround_walls, center=true);
                }
            }
        }
        // DIFFERENCE
        translate([vertical_arm_x_displacement+0.1,0, 40 + cone_hgt + vertical_arm_height]){
            rotate([0, 90, 0]){
                cylinder(bearing_width + 0.2, d=bearing_outer_diam, center=true);
            }
        }
    }
    if (include_bearing){
        translate([vertical_arm_x_displacement,0, 40 + cone_hgt + vertical_arm_height]){
            rotate([0, 90, 0]){
                translate([0, 0, -bearing_width / 2]){
                    bearing(model=bearing);
                }
            }
        }
    }

}

module drivegear(t1, t2, d, gear_h = 10, gear_shaft_h=5){
    // Renders drive gear
    // Accurate model https://www.aliexpress.com/item/554795482.html?spm=a2g0s.9042311.0.0.3acc4c4djddXuv
    // where t1=17, t2=37, d=15.6
    cp = 360*d/(t1+t2)*1.5;
    gear1_shaft_d = 5;  			// diameter of motor shaft
    gear1_shaft_r  = gear1_shaft_d/2;	
    // gear1 shaft assumed to fill entire gear.
    // gear1 attaches by means of a captive nut and bolt (or actual setscrew)
    gear1_setscrew_offset = 5;			// Distance from motor on motor shaft.
    gear1_setscrew_d         = 3.5;		
    gear1_setscrew_r          = gear1_setscrew_d/2;
    gear1_captive_nut_d = 6;
    gear1_captive_nut_r  = gear1_captive_nut_d/2;
    gear1_captive_nut_h = 2.5;
	g1p_d  =  t1 * cp / 180;
	g2p_d  =  t2 * cp / 180;
	g1p_r   = g1p_d/2;
	g2p_r   = g2p_d/2;
	difference()
	{
        gear(twist=0,
        	number_of_teeth=round(t1/1.5), 
        	circular_pitch=cp, 
        	gear_thickness = gear_shaft_h + (gear_h/2)+AT, 
        	rim_thickness = (gear_h/2)+AT, 
        	rim_width = 0.5,
        	hub_thickness = (gear_h/2)+AT, 
        	bore_diameter=0); 

    	//DIFFERENCE:
    	//shafthole
    	translate([0,0,-TT]) 
    		cylinder(r=gear1_shaft_r, h=gear_h+gear_shaft_h+ST, $fn=FN);

    	//setscrew shaft
    	translate([0,0,gear_h+gear_shaft_h-gear1_setscrew_offset])
    		rotate([0,90,0])
    		cylinder(r=gear1_setscrew_r, h=g1p_r, $fn=FN);

    	//setscrew captive nut
    	translate([(g1p_r)/2, 0, gear_h+gear_shaft_h-gear1_captive_nut_r-gear1_setscrew_offset]) 
    		translate([0,0,(gear1_captive_nut_r+gear1_setscrew_offset)/2])
    			#cube([gear1_captive_nut_h, gear1_captive_nut_d, gear1_captive_nut_r+gear1_setscrew_offset+ST],center=true);
    }
}

module ring_mount_holes(r=17.5, h=2.4, d1=2.85, ds=0){
    // Helper function for 90deg machine screw holes
    translate([r, 0, ds/2]){
        cylinder(ds, d1=d1+ds*2, d2=d1+ds*2, center=true, $fn=FN);
    }
    translate([r, 0, 0]){
        cylinder(h, d=d1, center=true, $fn=FN);
    }
    rotate([0, 0, 120]){
        translate([r, 0, ds/2]){
            cylinder(ds, d1=d1+ds*2, d2=d1+ds*2, center=true, $fn=FN);
        }
        translate([r, 0, 0]){
            cylinder(h, d=d1, center=true, $fn=FN);
        }
    }
    rotate([0, 0, -120]){
        translate([r, 0, ds/2]){
            cylinder(ds, d1=d1+ds*2, d2=d1+ds*2, center=true, $fn=FN);
        }
        translate([r, 0, 0]){
            cylinder(h, d=d1, center=true, $fn=FN);
        }
    }
}

module slip_ring(){
    // Accurate model of SRM-12-08D conductive 8 wire slip ring
    // https://www.aliexpress.com/item/32812638356.html?spm=a2g0s.9042311.0.0.71d24c4d5lWaA0
    translate([0,0,-1.1]){
        difference(){
            cylinder(2.2, d=44, center=true, $fn=FN);
            translate([0,0,-0.1]){
                ring_mount_holes(r=17.5, h=2.5, d1=5.3, ds=0);
            }
        }
        translate([0, 0, -9.7]){    
        cylinder(21.6, d=12.6, center=true, $fn=FN);
            translate([0, 0, -11.55]){
                cylinder(1.5, d=5, center=true, $fn=FN);
            }
        }
    }
}

module power_supply(){
    // Rough model of AIFENG 12v 5A 60W power supply
    // Mounting holes not exactly positioned
    difference(){
        color("grey"){
            cube([110, 77, 37]);
        }
        translate([-0.1, 1.1, 1.1]){
            cube([15, 76, 36]);
        }
        translate([102.1, 1.1, 1.1]){
            cube([8, 76, 36]);
        }
        translate([5.0,0.6, 29.5]){
            rotate([90, 0, 0]){
                cylinder(1.7, d=3.5, center=true, $fn=FN);
            }
        }
        translate([10,0.6, 20]){
            rotate([90, 0, 0]){
                cylinder(1.7, d=2.0, center=true, $fn=FN);
            }
        }
        translate([97.5,0.6, 15]){
            rotate([90, 0, 0]){
                cylinder(1.7, d=2.0, center=true, $fn=FN);
            }
        }
        translate([106,0.6,27.5]){
            rotate([90, 0, 0]){
                cylinder(1.7, d=3.5, center=true, $fn=FN);
                translate([0.5, -1.75, -1]){
                    cube([4.5, 3.5, 3.5]);
                }
            }
        }
        translate([4.5,5.0, 0.55]){
            rotate([0, 0, 0]){
                cylinder(1.7, d=3.5, center=true, $fn=FN);
            }
        }
        translate([108,72.5, 0.55]){
            rotate([0, 0, 0]){
                cylinder(1.7, d=3.5, center=true, $fn=FN);
                translate([0.5, -1.75, -1]){
                    cube([4.5, 3.5, 3.5]);
                }
            }
        }
    }

    color("orange"){
        translate([0, 20, 3]){
            cube([15, 40, 14]);
        }
    }
}

module support_column(r, deg, h=30, diam=post_diam, screw_diam=post_screw_diam){
    // Support post
    rotate([0, 0, deg]){
        translate([r, 0, -h/2]){
            difference(){
                cylinder(h, d=diam, center=true, $fn=FN);
                translate([0, 0, -0.1]){
                    cylinder(h+0.2, d=screw_diam, center=true, $fn=FN);
                }
            }
        }
    }
}

module motor_mounting_holes() {
    translate([69.7, -15.3, -0.1]){
                        cylinder(5.2, d=5, $fn=FN);
                        translate([0, 31, 0]){
                            cylinder(5.2, d=5, $fn=FN);
                        }
                        translate([-31, 0, 0]){
                            cylinder(5.2, d=5, $fn=FN);
                        }
                        translate([-31, 31, 0]){
                            cylinder(5.2, d=5, $fn=FN);
                        }
                    }
}

module material_reduction_hole(os=0)
{
    translate([0,25+os,-0.05]){
            cylinder(5.1, d=25, $fn=FN);
        }
        translate([15,40+os,-0.05]){
            cylinder(5.1, d=25, $fn=FN);
        }
        translate([-15,40+os,-0.05]){
            cylinder(5.1, d=25, $fn=FN);
        }
        translate([0,25+os,-0.05]){
        linear_extrude(height=5.1){
        polygon(points=[[-10,-8],[-25.5,8],[-17.5,27.5],[17.5,27.5], [25.5, 8], [10, -8]]);
        }
    }
}

module pivot(){
    difference(){
        union(){
            cube([65, 10, 2]);
            translate([0, 5, 0]){
                cylinder(d=10, h=2, $fn=50);
            }
            translate([65, 5, 0]){
                cylinder(d=10, h=2, $fn=50);
            }
            translate([0,5,2]){
                cylinder(d=7, h=2);
            }
            translate([5,5,2]){
                cylinder(d=7, h=2);
            }
            translate([10,5,2]){
                cylinder(d=7, h=2);
            }
            translate([65,5,2]){
                cylinder(d=5, h=8);
            }
        }
        translate([0, 5, -0.1]){
            cylinder(d=2.85, h=4.2);
        }
        translate([5, 5, -0.1]){
            cylinder(d=2.85, h=4.2);
        }
        translate([10, 5, -0.1]){
            cylinder(d=2.85, h=4.2);
        }
        translate([65, 5, -0.1]){
            cylinder(d=2.1, h=10.2);
        }
    }
}

module turret_platform(
    show_lower=true,
    show_mid=true,
    show_top=true,
    show_posts=true,
    show_accessories=true){
    // laser cut 5mm acrylic
    
    // stepper motor model
    if (show_accessories){
        translate([driven_shaft_position_r + 21.1, -20.95, 45+19.5]){
            rotate([-90.0, 0.0, 90]){
                color("teal"){
                    import("stl/motor_17HS4401.stl");
                }
            }
        }
        translate([-55, 55, 28]){
            rotate([180, 180, 90]){
                power_supply();
            }
        }
    }
     if (show_posts){
            color("brown"){
                post_height = 45.5;
                translate([0, 0, 24.5 + post_height]){
                    support_column(column_r, 60, h=post_height);
                    
                    difference(){
                    support_column(column_r, 180, h=post_height);
                    translate([-55,-7,-42.5]){
                        cube([10,15,38]);
                    }
                    }
                    
                    translate([17.5, -54, -45.5]){
                        difference(){
                    cylinder(h=3.5, d=12);
                            translate([0,0,-0.1]){
                                cylinder(h=3.7, d=2.8);
                            }
                        }
                    }
                    support_column(column_r, 300, h=post_height);
                }
            }
        }
    
    // upper assembly - holds slip ring inner part from turning
    if (show_top){
        translate([0,0,19.5]){
            color("lavender"){
                difference(){
                    union(){
                        difference(){
                        cylinder(5, d=140, $fn=FN);
                        // Material reduction holes
                        rotate([0, 0, 50]){
                            material_reduction_hole();
                        }
                        rotate([0, 0, 170]){
                            material_reduction_hole();
                        }
                        rotate([0, 0, 290]){
                            material_reduction_hole();
                        }
                    }
                        // stepper notch
                        translate([30, -22.5, 0]){
                            difference(){
                                roundedcube([50, 45, 5], false, 5, "z");
                                translate([-.1, -.1, 4.9]){
                                    cube([50.2, 45.2, 5.2]);
                                }
                                translate([-0.1, -0.1, -5.1]){
                                    cube([50.2, 45.2, 5.2]);
                                }
                            }
                        }
                    }
                    // Upper mounting structure support holes
                    rotate([0, 0, 90]){
                        translate([0, 0, 1.1]){
                            ring_mount_holes(r=column_r, h=5.5, ds=4);
                        }
                    }
                    mirror([0,0,1]){                        
                        rotate([0, 0, 60]){
                            translate([0, 0, -2.5]){
                                ring_mount_holes(r=column_r, h=5.5, ds=4);
                            }
                        }
                    }
                        
                    // servo shaft passthrough
                    //79.3
                    translate([driven_shaft_position_r, 0, -0.1]){
                        cylinder(5.2, d=25, $fn=FN);
                    }
                    // central hole for holding inner portion of slip ring
                    translate([0, 0, -0.1]){
                        cylinder(5.2, d=5, $fn=FN);
                    }
                    // motor mounting holes
                    motor_mounting_holes();
                    translate([1,0,0]){
                                            motor_mounting_holes();

                    }
                    translate([2,0,0]){
                                            motor_mounting_holes();

                    }
                    translate([3,0,0]){
                                            motor_mounting_holes();

                    }
                }
            }
        }
    }
    
    // upper support columns
    if (show_posts){
        color("brown"){
            translate([0, 0, 19.5]){
                support_column(column_r, 90, h=9.5);
                support_column(column_r, 210, h=9.5);
                support_column(column_r, 330, h=9.5);
            }
        }
    }
    
    // Mid section bearing support
    if (show_mid){
        color("blue"){
            translate([0, 0, 5.2]){
                difference(){
                    union(){
                        // main circle
                        difference(){
                        cylinder(5, d=140, $fn=FN);
                        // Material reduction holes
                        rotate([0, 0, 50]){
                            material_reduction_hole(5);
                        }
                        rotate([0, 0, 170]){
                            material_reduction_hole(5);
                        }
                        rotate([0, 0, 290]){
                            material_reduction_hole(5);
                        }
                    }                    }
                    // central hole for bearing
                    translate([0, 0, -0.1]){
                        cylinder(5.2, d=28, $fn=FN);
                    }
                    // Screw post mounting holes
                    mirror([0,0,1]){
                        rotate([0, 0, 90]){
                            translate([0, 0, -2.5]){
                                ring_mount_holes(r=column_r, h=5.5, ds=4);
                            }
                        }
                    }
                    // Screw post mounting holes
                    rotate([0, 0, 60]){
                        translate([0, 0, 1.1]){
                            ring_mount_holes(r=column_r, h=5.5, ds=4);
                        }
                    }
                    // drive gear passthrough
                    translate([55, 0, -0.1]){
                        cylinder(5.2, d=18, $fn=FN);
                    }

                }
            }
        }
    }
    
    // lower support columns
    if (show_posts){
        color("brown"){
            translate([0, 0, 5.2]){
                support_column(column_r, 60, h=9.5);
                support_column(column_r, 180, h=9.5);
                support_column(column_r, 300, h=9.5);
            }
        }
    }
    
    // lower support base
    if (show_lower){
        color("lime"){
            translate([0, 0, -9.3]){
                difference(){
                    union(){
                        // main circle
                        cylinder(5, d=turret_top_d, $fn=FN);
                    }
                    // central hole for rotating structure
                    translate([0, 0, -0.1]){
                        cylinder(5.2, d=75, $fn=FN);
                    }
                    // Screw post mounting holes
                    mirror([0,0,1]){
                        rotate([0, 0, 60]){
                            translate([0, 0, -2.5]){
                                ring_mount_holes(r=column_r, h=5.5, ds=4);
                            }
                        }
                    }
                    // drive gear passthrough
                    translate([55, 0, -0.1]){
                        cylinder(5.2, d=15, $fn=FN);
                    }
                }
            }
        }
    }
}

module turret_rotational_gear(diameter, h = 5, cp = 104, 
    show_thrust_bearing=true,
    show_axial_bearing=true,
    show_slip_ring=true,
    show_gear_wheel=true,
    show_drive_gear=true,
    show_lower_thrust_plate=true,
    show_armature=true,
    include_hull_arm_bearing = true,
    show_pivot = true){
    teeth = round(diameter*0.5*PI*(17/15.6)/1.5);
    if (show_gear_wheel){
        difference(){
            gear(twist=0,
                number_of_teeth=teeth, 
                circular_pitch=cp*1.5,
                gear_thickness = h, 
                rim_thickness = h, 
                rim_width = 0.5,
                hub_thickness = h, 
                bore_diameter=0);
            cylinder(h+6, d=15, center=true, $fn=FN);
            // slip ring mounting
            translate([0, 0, h/2]){
                ring_mount_holes(r=17.5, h=h + 0.5, ds=3.5);
            }
            // overstructure mounting holes
            rotate([0, 0, 60]){
                 translate([0, 0, h/2]){
                    ring_mount_holes(r=30, h=h + 0.5, ds=3.5);
                 }
             }
         }
     }
     // drive gear
     if (show_drive_gear){
         translate([driven_shaft_position_r, 0, 0]){
            drivegear(turret_rotational_drive_gear_teeth, 37, turret_rotational_drive_gear_diameter);
        }
    }

     // slip ring
     if (show_slip_ring){
         translate([0, 0, -2.2]){
            mirror([0, 0, 1]){
                color("purple"){
                    slip_ring();
                }
            }
        }
    }
    
    // slip ring bearing
    if (show_axial_bearing){
        translate([0, 0, h]){
            bearing(model=6001);
        }
    }
    
    // thrust bearing
    if (show_thrust_bearing){
        translate([0,0,-2]){
            rotate([0,90,0]){
                color("red"){
                    // invalid/broken model, disable when rendering
                    import("stl/AXK 75100.stl");
                }
            }
        }
    }
    
    // lower axial thrust containment plate
    plate_diam_outer = 74;
    plate_diam_inner = 45;
    if (show_lower_thrust_plate){
        translate([0, 0, -5]){
            difference(){
                color("skyblue"){
                    cylinder(5, d=plate_diam_outer, $fn=FN);
                }
                // passthrough for slip ring
                translate([0,0,-0.1]){
                    cylinder(5.2, d=plate_diam_inner, $fn=FN);
                }
                // overstructure mounting holes
                rotate([0, 0, 60]){
                     translate([0, 0, h/2]){
                        ring_mount_holes(r=30, h=h + 0.5, ds=0);
                     }
                }
            }
        }
    }

    
    // connecting support to gun bearing pivot
    if (show_armature){
        mirror([0,0,1]){
            translate([0, 0, cone_wall_thickness]){
                // 360' rotation cone
                difference(){
                    rotate_extrude($fn=FN) polygon( points=[
                    // Define rotational extrusion polygon
                    [cone_wire_passthrough_radius,cone_hgt],
                    [cone_upper_radius, cone_hgt],
                    [plate_diam_inner / 2, cone_wall_thickness],
                    [plate_diam_outer / 2, cone_wall_thickness],
                    [plate_diam_outer / 2, 0],
                    [plate_diam_inner / 2 - cone_wall_thickness,0],
                    [cone_upper_radius - cone_wall_thickness, cone_hgt - cone_wall_thickness],
                    [cone_wire_passthrough_radius, cone_hgt - cone_wall_thickness],
                    ]);
                    translate([0, 0, cone_wall_thickness/2]){
                        ring_mount_holes(r=30, h=cone_wall_thickness+0.2, ds=4);
                    }
                }
            }
            translate([0, 0, cone_hgt]){
                // connecting arms
                connecting_arm();
                mirror([1, 0, 0]){
                    connecting_arm();
                }
            }
            // Convex hull from arms to start of vertical arm sides
            hull_arm(include_bearing = include_hull_arm_bearing);
            mirror([1, 0, 0]){
                hull_arm(include_bearing = include_hull_arm_bearing);
            }
        }
    }
    
    // pivot arm from servo to armature
    if (show_pivot)
    {
        translate([35, 0, -140]){
            rotate([90, 140, 90]){
                color("blue"){
                    pivot();
                }
            }
        }
    }
}

module nerf_gun(nerf_gun_pitch){
    translate([0, nerf_gun_pivot_pt_y, -40 - cone_hgt + nerf_gun_pivot_pt_z - vertical_arm_height]){
        // Adjust to true pivot point
        w = vertical_arm_x_displacement * 2 + vertical_arm_w + 0.2;
        x = w / 2 + 0.1;
        translate([-x, -nerf_gun_pivot_pt_y, -nerf_gun_pivot_pt_z]){
            rotate([nerf_gun_pitch, 0, 0]){
                // Show pivot rod
                rotate([0,90,0]){
                    color("red"){
                        cylinder(w, d=pivot_rod_diameter);
                    }
                }
                translate([x, nerf_gun_pivot_pt_y, nerf_gun_pivot_pt_z]){
                    rotate([0, 90, 0]){
                        color("grey"){
                            import("nerfgun.off");
                        }
                    }
                }
            }
        }
    }
}

module shroud_vent_holes(){
    // Done using inverse hexagons for performance
    // This pattern does not like large intersection / difference operations
    // how big is each hexagon and padding block
    hexa_pattern_size = (hexagon_scale * 2 + hexagon_padding);
    // how big around is the pattern we want to draw (pi * d)
    circumferance = ((turret_top_d + print_wall_thickness * 2) * PI);
    radius = turret_top_d / 2 + print_wall_thickness;
    // How many rows
    rows = shroud_height / hexa_pattern_size;
    // How many columns per revolution
    cols = circumferance / hexa_pattern_size;
    // How many degrees per hexagon
    deg_inc = 360 / cols;
    // Downscale padding
    p = hexagon_padding / 2;
    s = hexagon_scale;
    q = 2;  // Adjust me when rotating hexagon pattern
    // Path data for polygon
    points = [
       // Hexagon (cutout)
       [.5 * s, -.9 * s],
       [-.5 * s, -.9 * s],
       [-1 * s, 0 * s],
       [-.5 * s, .9 * s],
       [.5 * s, .9 * s],
       [1 * s, 0 * s],
       [.5 * s, -.9 * s],
       // Surrounding area
       [-s - p, -s - p],
       [s + p, -s - p],
       [s + p, s + p],
       [-s - p - q, s + p],
       [-s - p - q, s + p - q],
       [-s - p, s + p - q],
    ];
    paths = [[7, 8, 9, 10, 11, 12], [0, 1, 2, 3, 4, 5, 6]];
    for(i = [-cols : 1 : round((rows + 1) * cols)]){
        rotation = (i * deg_inc * x_offset_hexagons) % 360;
        x = cos(rotation) * radius;
        y = sin(rotation) * radius;
        z = (i / cols) * hexa_pattern_size * y_offset_hexagons;
        translate([x, y, z]){
            rotate([90, hexagon_rotation, rotation - 90]){
                linear_extrude(height=print_wall_thickness){
                polygon(points, paths, convexity=1);    
                }
            }
        }
    }
}

module shroud(){
    h = (hexagon_scale * 2 + hexagon_padding * 2) * 1.5;
    solid_height = 5;   // Solid region top / bottom of shroud vents
    difference(){
        shroud_vent_holes();
        translate([0, 0, - h]){
            cylinder(h, d=turret_top_d + print_wall_thickness * 2 + 0.5, $fn=FN);
        }
        translate([0, 0, shroud_height]){
            cylinder(h, d=turret_top_d + print_wall_thickness * 2 + 0.5, $fn=FN);
        }
    }
    translate([0, 0, 0]){
        difference(){
            cylinder(solid_height, d=turret_top_d + print_wall_thickness * 2, $fn=FN);
            translate([0, 0, -0.1]){
                cylinder(solid_height + 0.2, d=turret_top_d, $fn=FN);
            }
        }
    }
    translate([0, 0, shroud_height - solid_height]){
        difference(){
            cylinder(solid_height, d=turret_top_d + print_wall_thickness * 2, $fn=FN);
            translate([0, 0, -0.1]){
                cylinder(solid_height + 0.2, d=turret_top_d, $fn=FN);
            }
        }
    }
}

module turret_assembly(
        show_stationary=true,
        show_rotating=true,
        show_turret_shroud=false,
        show_nerf_gun=true,
        show_shelf_mount=true,
        // Subcomponents
        show_lower=true,
        show_mid=true,
        show_top=true,
        show_posts=true,
        show_accessories=true,
        show_thrust_bearing=true,
        show_axial_bearing=true,
        show_slip_ring=true,
        show_gear_wheel=true,
        show_drive_gear=true,
        show_lower_thrust_plate=true,
        show_armature=true,
        include_hull_arm_bearing=true,
        nerf_gun_pitch=0){
    // 5mm laser cut acrylic and supplied 17 tooth drive gear
    
    // Rotating components
    if (show_rotating){
        turret_rotational_gear(
        turret_rotational_base_diameter,
        show_thrust_bearing=show_thrust_bearing,
        show_axial_bearing=show_axial_bearing,
        show_slip_ring=show_slip_ring,
        show_gear_wheel=show_gear_wheel,
        show_drive_gear=show_drive_gear,
        show_lower_thrust_plate=show_lower_thrust_plate,
        show_armature=show_armature,
        include_hull_arm_bearing=include_hull_arm_bearing);
    }
    
    // Fixed components
    if (show_stationary){
        turret_platform(
        show_lower=show_lower,
        show_mid=show_mid,
        show_top=show_top,
        show_posts=show_posts,
        show_accessories=show_accessories);
    }

    // Nerf gun
    if (show_nerf_gun){
        nerf_gun(nerf_gun_pitch);
    }
    
    // Shelf assembly
    // Shelf mount
    if (show_shelf_mount)
    {
        shelf_adapter();
    }
    
    // Shroud
    if (show_turret_shroud){
        stepper_cutout_padding = 10;
        stepper_cutout = 47;
        angle_padding = 10;
        angle = 40;
        z = 9;
        difference(){
            union(){
                translate([0, 0, -10]){
                    shroud();
                }
                translate([0, 0, z]){
                    rotate([0, 0, -(angle + angle_padding * 2)/2]){
                        rotate_extrude(angle = angle + angle_padding * 2, convexity = 2, $fn=FN) {
                            translate([turret_top_d / 2, 0, 0]){
                                square([print_wall_thickness, stepper_cutout + stepper_cutout_padding * 2]);
                            }
    translate([0, 0, z + stepper_cutout_padding]){
                rotate([0, 0, -angle/2]){
                    rotate_extrude(angle = angle, convexity = 2, $fn=FN) {
                        translate([turret_top_d / 2 - 0.25, 0, 0]){
                            square([print_wall_thickness + 0.5, stepper_cutout]);
    }
    }
    }
    }
    }
    }
    }
    }
    }
    }
}

module pegs(){
    translate([20,0,0])
    {   
    cylinder(d=7, h=9);
    translate([0,0,9]){
        cylinder(d=5.94, h=9);
    }

    }
    cylinder(d=10.4, h=9);
    translate([0,0,9]){
        cylinder(d=5.94, h=9);
    }
}

module shelf_adapter(){
    translate([0, 0, 70]){
        difference(){
            union(){
                cylinder(d=140, h=5, $fn=FN);
                translate([-70, -70, 0]){
                    cube([70, 70, 5]);
                }
            }
            rotate([0, 0, 50]){
                material_reduction_hole();
            }
            rotate([0, 0, 170]){
                material_reduction_hole();
            }
            rotate([0, 0, 290]){
                material_reduction_hole();
            }
            rotate([0, 0, 60]){
                translate([0, 0, 1.1]){
                    ring_mount_holes(r=column_r, h=5.5, ds=4);
                }
            }
        }
        translate([0, 0, 5]){
            translate([0, 0, 50]){
                difference(){
                    union(){
                        cylinder(d=140, h=4, $fn=FN);
                        translate([-70, -70, 0]){
                            cube([70, 70, 4]);
                        }
                    }
                    rotate([0, 0, 50]){
                        material_reduction_hole();
                    }
                    rotate([0, 0, 170]){
                        material_reduction_hole();
                    }
                    rotate([0, 0, 290]){
                        material_reduction_hole();
                    }
                    rotate([0, 0, 60]){
                        translate([0, 0, 1.1]){
                            ring_mount_holes(r=column_r, h=8.5, ds=0, d1=10);
                        }
                    }
                }
            }
        }
        translate([-70, -70, 0]){
            difference(){
                cube([4, 70, 58]);
                translate([4.05, 10, 10]){
                    rotate([0, -90, 0]){
                    linear_extrude(height=4.1){
                    polygon(points=[[0,0],[38, 0], [38, 40], [19, 50], [0, 40]]);
                        }
                    }
                }
            }
        }
        translate([-70, -70, 0]){
            difference(){
                cube([70, 4, 58]);
                translate([10, -0.05, 10]){
                    cube([50, 4.1, 38]);
                }
            }
        }
    }
}

// Nerf gun pitch -40 to +50
turret_assembly(nerf_gun_pitch = 0);
//support_column(column_r, 300, h=9.5);

// TODO
// add screw holes to lower thrust plate for armature
// add pivot angle math
// swap blanks for pegs on hull arm bearing
// add servo to gun model
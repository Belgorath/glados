var socket = io.connect('/detector');
var removed_person_timeout = 1000;
var data_people = {};           // Live view including unregistered people
var data_all_people = {};       // Stored database profiles
var active_tab = null;
$(document.body).ready(function(){
    // Connect click event in image view
    $('#video-element').click(function(e){
        var y = (e.offsetY / this.height) * 100;
        var x = (e.offsetX / this.width) * 100
        socket.emit('fireat', {x: x, y: y});
    });
    // Connect tab change listener
    document.getElementById('tabbar').addEventListener('prechange', function(e) {
        active_tab = e.tabItem.page;
    });
    active_tab = document.querySelector('div.tabbar [active]').page;
    tabChangeUpdate(active_tab);
});
function updateVideoFeedThumbnails(){
    // Updates list of video thumbnails
    var date = new Date(); var timestamp = date.getTime();
    Object.values(data_people).forEach(function(person){
        var img = document.getElementById("person-" + person.id);
        if (!img){
            img = document.createElement("div");
            img.classList.add('person');
            img.id = "person-" + person.id;
            document.getElementById('people-preview').append(img);
            $(img).bind('click', {person_id:person.id}, function(e){onPersonClicked(e.data.person_id);});
        }
        img.classList.remove('invalid');
        img.updated_at = timestamp;
        encodeImageToBackground(img, person.image);
    });
    document.getElementById('people-preview').childNodes.forEach(function(person){
        if (timestamp != person.updated_at){
            if (timestamp - person.updated_at > removed_person_timeout){
                $(person).slideUp(300, function(){$(this).remove();});
            } else {
                person.classList.add('invalid');
            }
        }
    });
}
function updatePeopleList(){
    // Updates list of people
    var list = document.getElementById('people-list');
    $(list).children('ons-list-item').remove();
    Object.values(data_all_people).forEach(function(person){
        // Get status (in view)
        var current_person = data_people[person.id];
        var color = 'black';
        if (current_person){
            color = 'rgb(' + current_person.box_color.join(',') + ')'
        }
        var onClick = function(person, profile){
            var modal = document.getElementById('saved-person-modal');
            modal.getElementsByTagName('h3')[0].innerText = profile.name;
            modal.show();
            setupModal(modal);
        }
        list.appendChild(generateListItem(person, color, onClick));
    });
}
function tabChangeUpdate(active_tab){
    // Called on load and when tab menu changes
    switch(active_tab){
        case 'video-feed':
            updateVideoFeedThumbnails();
            break;
        case 'people':
            updatePeopleList();
            break;
        case 'profile':
            break;
    }
}
socket.on('all_people', function(data) {
    data_all_people = data.all_people;
});
socket.on('current_people', function(data) {
    // Handle updating list of detected people
    var new_data = {}
    data.current_people.forEach(function(person){
        new_data[person.id] = person;
    });
    data_people = new_data;
    // Perform update based on current tab
    switch(active_tab){
        case 'video-feed':
            updateVideoFeedThumbnails();
            break;
        case 'people':
            updatePeopleList();
            break;
        case 'profile':
            break;
    }
});
function onPersonClicked(person_id){
    // Handler for person modal dialog
    var person = data_people[person_id];
    if (!person){return;}
    var modal = document.getElementById('person-preview-modal');
    modal.getElementsByClassName('person')[0].style.backgroundImage = document.getElementById('person-' + person.id).style.backgroundImage;
    modal.getElementsByClassName('action-fire')[0].onclick = function(){fireNerfAtPerson(person);removeModal(modal);}
    modal.getElementsByTagName('h3')[0].innerText = data_all_people[person_id] ? data_all_people[person_id].name : 'Unknown Person';
    var addButton = modal.getElementsByClassName('action-add')[0];
    var updateButton = modal.getElementsByClassName('action-update')[0];
    var faceWarning = modal.getElementsByClassName('facial-recognition-warning')[0];
    if (person.facial_encoding){
        faceWarning.style.display = 'none';
        if (person.known_person){
            addButton.style.display = 'none';
            updateButton.style.display = 'block';
        } else {
            addButton.style.display = 'block';
            updateButton.style.display = 'none';
        }
    } else {
        addButton.style.display = 'none';
        updateButton.style.display = 'none';
        faceWarning.style.display = 'block';
    }
    addButton.onclick = function(){addPerson(person);removeModal(modal);}
    updateButton.onclick = function(){addFaceToPerson(person, person.facial_encoding);removeModal(modal);}
    modal.show();
    setupModal(modal);
}
function addPerson(person){
    // Handler for add person modal dialog
    var modal = document.getElementById('new-person-modal');
    var list = document.getElementById('existing-people');
    var input = document.getElementById('new-user-name');
    var addButton = modal.getElementsByClassName('action-add')[0];
    addButton.onclick = function(){
        var name = input.value;
        if (!name.trim()){
            ons.notification.alert("Name field is required");
            return;
        }
        addPersonToDb(person, name, person.facial_encoding);
        removeModal(modal);
    }
    function onSelectExistingPerson(_person){
        // Called when user selects existing person
        addFaceToPerson(_person, person.facial_encoding);
    }
    setupModal(modal);
    input.onkeyup = function(e){
        var keyCode = (event.which || event.keyCode);
        if (keyCode == 13){
            return addButton.onclick();
        }
        populatePeopleList(data_all_people, list, onSelectExistingPerson, input.value)
    };
    input.onkeyup();
    setTimeout(function(){$("input", input).focus()}, 1000);
    modal.show();
}
// Model / Remote functions
function addPersonToDb(person, name, facial_encoding){
    socket.emit('addPersonToDb', {'person_id': person.id, 'name': name, 'facial_encoding': facial_encoding});
}
function addFaceToPerson(person, facial_encoding){
    socket.emit('addFaceToPerson', {'person_id': person.id, 'facial_encoding': facial_encoding});
}
function fireNerfAtPerson(person){
    socket.emit('fireNerfAtPerson', {'person_id': person.id});
}
function fireNerfRelTarget(x, y){
    socket.emit('fireNerfRelTarget', {x: x, y: y});
}
// Helper functions
function encodeImageToBackground(element, image){
    element.style.backgroundImage = "url(\"data:image/jpg;base64," + image+'\")';
}
function populatePeopleList(data, list, onClick, filter){
    // Populates list of people with optional name filter
    $(list).children('ons-list-item').remove();
    var matched_items = 0;
    Object.values(data_people).forEach(function(person){
        if (!person.known_person){return;}  // Skip temporary id people
        if (filter && !(data_all_people[person.known_person].name || '').toLowerCase().includes(filter.toLowerCase().trim())){
            return;
        }
        matched_items++;
        list.appendChild(generateListItem(person, 'black', onClick));
    });
    if (matched_items){
        $(list).slideDown(300);
    } else {
        $(list).slideUp(300);
    }
}
function generateListItem(person, color, onClick){
    if (!color){color = 'black';}
    var listItem = document.createElement('ons-list-item');
    var listItemIcon = document.createElement('ons-icon');
    listItemIcon.setAttribute('icon',"md-face");
    listItemIcon.style.color = color;
    listItemIcon.classList.add('list-item__icon');
    listItemIconContainer = document.createElement('div');
    listItemIconContainer.classList.add('left');
    listItemIconContainer.append(listItemIcon);
    listItem.append(listItemIconContainer);
    var listItemTitle = document.createElement('div');
    listItemTitle.innerText = person.name;
    listItemTitle.classList.add('center')
    listItem.append(listItemTitle);
    listItem['data-person'] = person.id;
    listItem.onclick = function(){onClick(data_people[this['data-person']], data_all_people[this['data-person']]);}
    return listItem;
}
function setupModal(modal){
    // Enables escape to close
    modal.setAttribute('tabindex', 0);
    modal.focus();
    modal.onkeyup = function(e){
        var keyCode = (event.which || event.keyCode);
        if (keyCode == 27){
            removeModal(modal);
        }
    }
    modal.getElementsByClassName('modal-closer')[0].onclick = function(){removeModal(modal);}
    document.body.classList.add('modal-open');
}
function removeModal(modal){
    $(modal).slideUp(200, function(){modal.hide();});
    document.body.classList.remove('modal-open');
}
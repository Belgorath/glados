import time

class Detector:
    def __init__(self, db):
        # DB should be instance of PyMongo database
        self.db = db
        
        # Import face recognition (slow)
        import base64
        import cv2
        import cvlib
        import face_recognition
        
        # Define overlay colors (BGR)
        self.color = (0, 255, 0)
        self.matched_color = (0, 255, 0)
        self.tracking_lost_color = (0, 255, 255)
        self.unknown_person = (255, 0, 0)
        self.frame_skip = 10
        self.frame_skip_counter = 0
        # Define overlay text styles (font, size, color, thickness)
        self.styles = {
            "default": [cv2.FONT_HERSHEY_SIMPLEX, 0.65, self.color, 1]
        }
        # Load stored data
        self.people = {}
        for person in self.db.people.find():
            _id = str(person['_id'])
            _person = {}
            _person.update(person)
            del _person['_id']
            _person['id'] = _id
            self.people[_id] = _person
        # Use list for faces as index position is used by matching lib
        self.faces = []
        for face in self.db.faces.find():
            _id = str(person['_id'])
            _face = {}
            _face.update(face)
            del _face['_id']
            _face['id'] = _id
            self.faces.append(_face)
        # Store current live view
        self.current_people = []
        # Counter of temporary unique ids that are negative so as to not
        # overlap any mongodb _id values
        self.id_ptr = -1
        # Time to retain a person in visual memory when loss of detection occurs (s)
        self.persistence_of_vision_timeout = 5
        # Boundary inside the viewing area (%) at which persistance is indefinite - i.e.
        # tracking can be lost after n seconds at edge of view, but in center view we assume
        # person is turned away from camera and didn't just vanish
        # This can also be None to disable
        # FIXME: Make this motion dependent in region with indefinate persistance
        self.indefinite_persistance_boundary = 0.2
        # Facial encoding oversize - pixels of padding around detected area to include
        self.facial_encoding_oversize = 25

    def process_frame(self, frame):
        # Process and return detected regions
        # Frame may be modified here to display detected regions
        self.frame_skip_counter += 1
        if self.frame_skip_counter < self.frame_skip:
            return self.current_people
        self.frame_skip_counter = 0
        people = []
        height, width = frame.shape[:2]
        overlap_padding = min(height, width) * 0.02     # Allow 2% per-frame POV overlap padding
        # Use fast DNN to get face regions in global image
        faces, confidences = cvlib.detect_face(frame)
        for index, (x1, y1, x2, y2) in enumerate(faces):
            # Clamp region at image bounds
            x1 = max(0, x1 - self.facial_encoding_oversize)
            y1 = max(0, y1 - self.facial_encoding_oversize)
            x2 = min(width - 1, x2 + self.facial_encoding_oversize)
            y2 = min(height - 1, y2 + self.facial_encoding_oversize)
            # Skip zero or inverse size frames
            if x2 - x1 <= 0 or y2 - y1 <= 0: continue
            person = {
                'position': (x1, y1, x2, y2),
                'facial_encoding': None,
                'persistant': {          # Persistant data between frames stored here
                    'confidence': int(confidences[index] * 100),
                },
                'box_color': self.unknown_person, # Box drawn around person can specify a color based on vision perception state
                'box_label': "Unknown person",
                'known_person': None,
                'known_facial_profile': None,
                'id': None
            }
            # Crop pixels in detected region
            face_image = frame[y1:y2, x1:x2]
            # Save as base64 encoded representation for use in frontend
            ret, image_data = cv2.imencode('.jpg', face_image)
            person['image'] = base64.b64encode(image_data.tobytes())
            # Get facial feature detection map
            facial_encodings = face_recognition.face_encodings(face_image)
            if len(facial_encodings):
                facial_encoding = facial_encodings[0]
                person['facial_encoding'] = facial_encoding.tolist()
                # Perform lookup against known dataset
                known_faces = map(lambda face: face['facial_encoding'], self.faces)
                matches = face_recognition.compare_faces(known_faces, facial_encoding)
                match_index = -1
                try:
                    match_index = matches.index(True)
                except ValueError:
                    pass
                
                if match_index > -1:
                    # Ensure that matched person is not already in list of people (avoid duplicate profile identification)
                    matched_person_id = self.faces[match_index]['person_id']
                    for _person in people:
                        if _person['id'] == matched_person_id:
                            match_index = -1
                            break

                if match_index > -1:
                    # Person is matched
                    person['id'] = self.faces[match_index]['person_id']
                    person['known_person'] = matched_person_id
                    person['box_label'] = self.people[matched_person_id]['name']
                    person['known_facial_profile'] = self.faces[match_index]
                    person['box_color'] = self.matched_color
                    # Get persistant data from current people if exists
                    for _person in self.current_people:
                        if _person['id'] == person['id']:
                            # ID match, mirror persistant data
                            update_persistance_data = person['persistant'].copy()
                            person['persistant'] = _person['persistant']
                            person['persistant'].update(update_persistance_data)
                else:
                    # Person is unmatched by facial recognition, check previous frame for persistance of vision
                    person['id'] = None
            # Delete lost_at timestamp if exists
            if 'lost_at' in person['persistant']:
                del person['persistant']['lost_at']
            people.append(person)

        # Perform processing for unknown people
        # This allows face detection dropouts to match box position (i.e. someone turned away from camera after detection)
        # There is no timeout on this
        for person in people:
            if person['id'] is None:
                overlap_person = None   # Include POV only when one person is in region
                for _person in self.current_people:
                    if self.regions_get_overlap(person['position'], _person['position'], overlap_padding):
                        # Only allow one overlap region
                        if overlap_person:
                            overlap_person = None
                            break
                        overlap_person = _person
                        # Only allow match of person who isn't already in results list (detected or virtual id)
                        for __person in people:
                            if __person is not person and _person['id'] == __person['id']:
                                overlap_person = None
                                break
                        if overlap_person == None:
                            break
                        # Person can be considered matched by overlap region, copy data
                        person['id'] = _person['id']
                        person['box_color'] = self.unknown_person if person['id'] < 0 else self.matched_color
                        person['box_label'] = _person['box_label']
                        person['known_person'] = _person['known_person']
                        person['known_facial_profile'] = _person['known_facial_profile']
                        update_persistance_data = person['persistant'].copy()
                        person['persistant'] = _person['persistant']
                        person['persistant'].update(update_persistance_data)
                        if _person['facial_encoding'] and not person['facial_encoding']:
                            person['facial_encoding'] = _person['facial_encoding']

        # Handle vision persistance between frames
        now = time.time()
        for person in people:
            if not 'detected_at' in person['persistant']:
                person['persistant']['detected_at'] = now

        if len(people) < self.current_people:
            for person in self.current_people:
                # Confirm person not listed by id
                found_by_id = False
                for _person in people:
                    if person['id'] == _person['id']:
                        found_by_id = True
                        break
                if found_by_id:
                    break
                # Confirm person not overlapping other
                found_by_overlap = False
                for _person in people:
                    if self.regions_get_overlap(_person['position'], person['position'], overlap_padding):
                        found_by_overlap = True
                        break
                if found_by_overlap:
                    break
                # Person has disappeared from tracking
                # Person will timeout after n seconds
                if self.persistence_of_vision_timeout:
                    if not 'lost_at' in person['persistant']:
                        person['persistant']['lost_at'] = now
                    if now - person['persistant']['lost_at'] < self.persistence_of_vision_timeout:
                        # Copy person across through vision persistance
                        person['box_color'] = self.tracking_lost_color
                        people.append(person)
                    else:
                        # Once timeout occurs, only retain person if within bounding long-term persistance region
                        if self.indefinite_persistance_boundary:
                            cx = (person['position'][0] + person['position'][2]) // 2
                            cy = (person['position'][1] + person['position'][3]) // 2
                            if cx > width // 2: cx -= width
                            if cy > height // 2: cy -= height
                            if abs(cx) > width * self.indefinite_persistance_boundary and abs(cy) > height * self.indefinite_persistance_boundary:
                                # Person inside indefinite persistance boundary and will not time out
                                person['box_color'] = self.tracking_lost_color
                                people.append(person)
        
        # Generate new IDs for any unmatched people
        for person in people:
            if not person['id']:
                person['id'] = self.get_id()
        
        # Draw on the frame detection results
        for person in people:
            # Draw rectangle around detected face
            cv2.rectangle(frame, (person['position'][0], person['position'][1]), (person['position'][2], person['position'][3]), person['box_color'], 2)
            # Draw confidence value and text
            self.write_text(frame, person['position'][0], person['position'][3] + 25, str(person['persistant']['confidence']) + "%" + " - " + person['box_label'])

        # Write to current people data
        self.current_people = people
        return people

    def add_person(self, person_id, name, facial_encoding, data={}):
        # Adds new person to database
        inserted_data = {}
        # Only allow adding of temporary ids
        if person_id > -1: return
        # If converting an existing person, store their current live profile
        if person_id in self.current_people:
            live_data = self.current_people[person_id]
            del self.people[person_id]
            inserted_data.update(live_data['persistant_data'])
            live_data['name'] = name
        else:
            live_data = {
                'name' : name
            }
        # Build data object
        inserted_data.update(data)
        inserted_data['name'] = name
        # Insert and propagate id
        new_person_id = self.db.people.insert_one(inserted_data).inserted_id
        new_person_id = str(new_person_id)
        live_data['id'] = new_person_id
        self.people[new_person_id] = live_data
        self.add_face(new_person_id, facial_encoding)
        return person_id
    
    def add_face(self, person_id, facial_encoding):
        # Adds new face to existing person
        face = {
            'person_id': person_id,
            'facial_encoding': facial_encoding
        }
        face_id = self.db.faces.insert_one(face.copy()).inserted_id
        face_id = str(face_id)
        face['id'] = face_id
        self.faces.append(face)
        return face_id

    def write_text(self, frame, x, y, text, style="default"):
        # Define text style
        cv2.putText(frame, text, (x, y), *self.styles[style])

    def regions_get_overlap(self, region1, region2, overlap_padding=0):
        # Returns true if regions overlap
        # Uses separating axis theorem
        # overlap_padding allows more or less generous overlap tolerances
        return (region1[0] < region2[2] + overlap_padding or\
                region1[2] + overlap_padding > region2[0] or\
                region1[1] < region2[3] + overlap_padding or\
                region1[3] + overlap_padding > region2[1])
        
    def get_people(self):
        return self.people
    
    def get_current_people(self):
        return self.current_people

    def get_id(self):
        # Get unique id
        self.id_ptr -= 1
        return self.id_ptr
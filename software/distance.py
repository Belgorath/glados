import serial
import time

class TFMini:
    def __init__(self, port="/dev/ttyAMA0", baud=115200, timeout=5):
        self.sensor_initialized = False
        self.baud = baud
        self.port = port
        self.timeout = timeout
        self.serial = None
        self.distance = 0 
        try:
            self.serial = serial.Serial(self.port, self.baud, timeout=self.timeout)
            initialization_bytes = bytearray([0x42, 0x57, 0x02, 0x00, 0x00, 0x00, 0x01, 0x06])
            self.serial.write(initialization_bytes)
            self.sensor_initialized = True
        except Exception, e:
            print "Could not open serial port", e

    def get_distance(self):
        # Waits for packet from device and returns data
        start = time.time()
        while time.time() - start < self.timeout:
            if self._get_next_distance_if_avail():
                return self.distance
                
    def get_distance_async(self):
        # Returns next available distance or previous distance if device not ready
        self._get_next_distance_if_avail()
        return self.distance
    
    def _get_next_distance_if_avail(self):
        # Internal function to poll device and record depth
        if not self.serial: return
        # Ensure we are reading most recent status
        if self.serial.in_waiting > 18:
            self.serial.read(self.serial.in_waiting - 17)
        while(self.serial.in_waiting >= 9):
            if(('Y' == self.serial.read()) and ('Y' == self.serial.read())):
                dist_l = self.serial.read()
                dist_h = self.serial.read()
                dist_total = (ord(dist_h) * 256) + (ord(dist_l))
                for i in range (0, 5):
                    self.serial.read()
                self.distance = dist_total
                return self.distance
                
if __name__ == "__main__":
    tfmini_instance = TFMini()
    while True:
        print tfmini_instance.get_distance()
        
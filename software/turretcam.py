from aiming import Aiming
from auth import Authentication
from camera import Camera
from detector import Detector
from distance import TFMini
import argparse
import cv2
from flask import Flask, render_template, Response
from flask_socketio import SocketIO, emit
from pymongo import MongoClient
from threading import Thread

# pip install facenet, cv2, numpy, flask, face_recognition, tensorflow, opencv-python, cvlib, pymongo, flask-socketio, eventlet

class TurretCam:
    def __init__(self, args):
        # Unpack arguments
        self.camera_index = args.camera_id
        self.debug = args.debug
        self.enable_facial_recognition = args.enable_facial_recognition
        self.distance_sensor_port = args.distance_sensor_port

        self.mongo_username = args.mongo_username
        self.mongo_password = args.mongo_password
        self.mongo_host = args.mongo_host
        self.mongo_port = args.mongo_port        
        
        # Constants
        self.default_player_profile = {
            # Player stats (protected)
            'stats': {
                'shots': 0,             # Number of shots fired
                'hits': 0,              # Number of hits taken
            },
            # Settings (unprotected)
            'settings': {
                'fireOnSight': False    # Flag to fire when person first detected
            }
        }

        # Start mongodb
        if self.mongo_username:
            self.mongo_client = MongoClient('mongodb://%s:%s@%s:%d' % (self.mongo_username, self.mongo_password, self.mongo_host, self.mongo_port))
        else:
            self.mongo_client = MongoClient('mongodb://%s:%d' % (self.mongo_host, self.mongo_port))

        self.db = self.mongo_client.TurretCam

        # Create helper class instances
        self.aiming_instance = Aiming()
        self.auth_instance = Authentication(self.db, self.default_player_profile)
        self.camera_instance = Camera(camera_index=self.camera_index)
        self.distance_instance = TFMini(self.distance_sensor_port)
        self.detector_instance = None
        if self.enable_facial_recognition:
            self.detector_instance = Detector(self.db)        

    # Data transmission helpers
    def get_all_people(self):
        # Get all stored profiles
        # Done through single function to ensure only data client should see is being presented
        data_to_send = {}
        if not self.enable_facial_recognition: return data_to_send
        people = self.detector_instance.get_people()
        for person_id in people:
            data_item = people[person_id].copy()
            if 'email' in data_item:
                del data_item['email']
            if 'password' in data_item:
                del data_item['password']
            data_to_send[person_id] = data_item
        return data_to_send

    def get_current_people(self):
        # Get active camera tracking targets including attached profiles
        # Done through single function to ensure only data client should see is being presented
        if not self.enable_facial_recognition: return []
        return self.detector_instance.get_current_people()

    def get_frame(self):
        # Get next camera frame
        frame = self.camera_instance.get_frame()
        people = []
        distance = 0
        if frame is not None:
            # Process detection of people
            if self.enable_facial_recognition:
                people = self.detector_instance.process_frame(frame)
            # Get distance to target
            distance = self.distance_instance.get_distance_async()
            # Add features to frame image
            self.aiming_instance.draw_crosshairs(frame, distance)
        return frame, people, distance

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Turret Camera')
    parser.add_argument('--camera', type=int, dest="camera_id", default=0)
    parser.add_argument('--distance-sensor-port', dest="distance_sensor_port", default="/dev/ttyAMA0")
    parser.add_argument('--port', type=int, dest="port", default=8080)
    parser.add_argument('--mode', dest="mode", default="webserver")
    parser.add_argument('--mongo-host', dest="mongo_host", default="localhost")
    parser.add_argument('--mongo-username', dest="mongo_username")
    parser.add_argument('--mongo-password', dest="mongo_password")
    parser.add_argument('--mongo-port', type=int, dest="mongo_port", default=27017)
    parser.add_argument('--debug', dest="debug", default=False, type=lambda x: (str(x).lower() in ['true', '1', 'yes']))
    parser.add_argument('--enable-facial-recognition', dest="enable_facial_recognition", default=True, type=lambda x: (str(x).lower() in ['true', '1', 'yes']))
    args = parser.parse_args()

    # Create turretcam instance
    turretcam = TurretCam(args)

    # Start flask webserver
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'MA9sCcLpymcH5cOiChvV9aUZxYSTTdnW'
    socketio = SocketIO(app, async_mode='threading')

    # Handlers for http static routes
    @app.route('/')
    def index():
        return render_template('index.html')

    @app.route('/index.js')
    def index_js():
        return render_template('index.js')

    @app.route('/index.css')
    def index_css():
        return Response(render_template('index.css'),
            mimetype='text/css')

    # Handlers for websocket endpoints
    @socketio.on('connect', namespace='/detector')
    def on_connect():
        print "New client connected"
        emit('current_people', {'current_people': turretcam.get_current_people()}, broadcast=False)
        emit('all_people', {'all_people': turretcam.get_all_people()}, broadcast=False)

    @socketio.on('addPersonToDb', namespace='/detector')
    def add_person(data):
        data.update(default_player_profile)
        turretcam.detector_instance.add_person(data['person_id'], data['name'], data['facial_encoding'])
        emit('all_people', {'all_people': turretcam.get_all_people()}, broadcast=True)

    @socketio.on('addFaceToPerson', namespace='/detector')
    def add_face_to_person(data):
        turretcam.detector_instance.add_face(data['person_id'], data['facial_encoding'])

    @socketio.on('fireNerf', namespace='/detector')
    def fire_nerf(data):
        print "Fire nerf!"
        print person_id

    def video_feed_loop():
        while True:
            # Get camera frame
            frame, people, distance = turretcam.get_frame()
            # Update connected clients
            with app.test_request_context('/detector'):
                emit('current_people', {'current_people': turretcam.get_current_people()}, namespace="/detector", broadcast=True)
            ret, jpeg = cv2.imencode('.jpg', frame)
            yield (b'--frame\r\n'
                b'Content-Type: image/jpeg\r\n\r\n' + jpeg.tobytes() + b'\r\n\r\n')

    @app.route('/video_feed')
    def video_feed():
        # Initialize mjpeg stream
        frame = video_feed_loop()
        return Response(frame,
                        mimetype='multipart/x-mixed-replace; boundary=frame')

    def window_mode():
        while True:
            # Get camera frame
            frame, people, distance = turretcam.get_frame()
            cv2.imshow('frame', frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        cv2.destroyAllWindows()
    
    if args.mode == "webserver":
        socketio.run(app, host='0.0.0.0', port=args.port, debug=args.debug)
    elif self.mode == "window":
        window_mode()
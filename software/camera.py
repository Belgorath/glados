import cv2
import numpy as np
class Camera:
    def __init__(self, camera_index = -1):
        self.video = None
        self.overlay_frame = None
        
        self.camera_width = 640
        self.camera_height = 480
        self.width = None
        self.height = None
        self.mapx = {}
        self.mapy = {}
        
        self.camera_index = camera_index
        if self.camera_index == -1:
            camera_indicies = self.get_camera_indicies()
            if camera_indicies:
                self.camera_index = camera_indicies[0]
        if self.camera_index > -1:
            # Camera matrix
            self.camera_K = np.array([[673.9683892, 0., 343.68638231],
                    [0., 676.08466459, 245.31865398],
                    [0., 0., 1.]])

            # Define distortion coefficients d
            self.camera_d = np.array([5.44787247e-02, 1.23043244e-01, -4.52559581e-04, 5.47011732e-03, -6.83110234e-01])

            self.video = cv2.VideoCapture(self.camera_index)
            self.camera_width, self.camera_height = (self.video.get(3), self.video.get(4))
            if self.width is None:
                self.width = self.camera_width
                self.height = self.camera_height
            self.generate_distortion_tables()
        else:
            self.overlay_frame = self.overlay_message("Camera not found")

    def generate_distortion_tables(self):
        # Calculate distortion correction lookup tables

        # Generate new camera matrix from parameters
        newcameramatrix, roi = cv2.getOptimalNewCameraMatrix(self.camera_K, self.camera_d, (int(self.width),int(self.height)), 0)

        # Generate look-up tables for remapping the camera image
        self.mapx, self.mapy = cv2.initUndistortRectifyMap(self.camera_K, self.camera_d, None, newcameramatrix, (int(self.width),int(self.height)), 5)

    def get_undistorted_point(self, x, y):
        # Unmaps lense distortion to linear x/y for use in trajectory math
        return (self.mapx[int(x)], self.mapy(int(y)))

    def get_detected_objects(self):
        # Return list of detected objects
        return self.detected_objects

    def overlay_message(self, text):
        # Create a black image
        img = np.zeros((self.height,self.width,3), np.uint8)

        # Write some Text
        font                   = cv2.FONT_HERSHEY_SIMPLEX
        pos                    = (10,40)
        fontScale              = 1
        fontColor              = (255,255,255)
        lineType               = 2

        cv2.putText(img,text, 
            pos, 
            font, 
            fontScale,
            fontColor,
            lineType)
        return img

    def get_camera_indicies(self, desired=1):
        # Returns array of valid camera indicies
        index = 0
        arr = []
        retries = 10
        while True and index < 10:
            cap = cv2.VideoCapture(index)
            if not cap.read()[0]:
                break
            else:
                arr.append(index)
                if len(arr) == desired:
                    break
            cap.release()
            index += 1
        return arr

    def get_frame(self):
        # Returns video frame or overlay if defined
        if self.overlay_frame is not None:
            return self.overlay_frame
        elif self.video:
            ret, frame = self.video.read()
            if (self.camera_width, self.camera_height) != (self.width, self.height):
                frame = cv2.resize(frame,(self.width,self.height))
            return frame
        return None

    def open(self):
        if not self.video.isOpened():
            self.video.open(self.camera_index)

    def status(self):
        return not self.video.isOpened()
    
    def stop(self):
        if self.video.isOpened():
            self.video.stop()

    def __del__(self):
        if self.video:
            self.video.release()
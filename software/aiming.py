import cv2

class Aiming:
    def __init__(self, color=(0,0,255), line_width=2):
        # Define overlay text styles (font, size, color, thickness)
         self.line_width = line_width
         self.color = color
         self.styles = {
            "default": [cv2.FONT_HERSHEY_SIMPLEX, 0.65, self.color, 1]
        }
 

    def write_text(self, frame, x, y, text, style="default"):
        # Define text style
        cv2.putText(frame, text, (x, y), *self.styles[style])
        
    def draw_crosshairs(self, frame, distance):
        # Draws crosshairs on frame
        height, width = frame.shape[:2]
        length = int(min(height, width) * 0.25)
        x, y = width // 2, height // 2
        cv2.line(frame,(x,y),(x+length, y),self.color,self.line_width)
        cv2.line(frame,(x,y),(x-length, y),self.color,self.line_width)
        cv2.line(frame,(x,y),(x, y+length),self.color,self.line_width)
        cv2.line(frame,(x,y),(x, y-length),self.color,self.line_width)
        # Draw text indicating distance to target
        if distance:
            # Draw confidence value and text
            self.write_text(frame, x - length, y + length + 15, str(round(float(distance) / 100.0, 2)) + " meters")
            
    
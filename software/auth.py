import hashlib
class Authentication:
    def __init__(self, db, default_player_profile):
        self.db = db
        self.sessions = {}           # Local cache of sessions and their person_id / permissions
        self.salt = "S4frSeKdGAxjSgncypDWowlg7zpgRmPr"
        self.global_permissions = {  # Global permissions for all users
            "default": False,
            "fire": True,
            "tag": False,
            "signup": True
        }
        self.new_user_roles = {      # Roles granted to new users by default
            "default": True,
            "fire": True,
            "tag": True,
            "signup": True
        }
        self.default_player_profile = default_player_profile

    def verify_permission(self, session_id, role="default"):
        # Verify user can perform role / task
        # Role can be default, administer, etc
        if role in self.global_permissions:
            if self.global_permissions[role]:
                return True
        session = self.verify_user(session_id)
        if session:
            if 'roles' in session:
                if role in session['roles']:
                    return session['roles'][role]
        return False   

    def verify_user(self, session_id):
        # Returns person_id or False
        # Person data contains role information
        if session_id not in self.sessions:
            session = self.db.sessions.fetch_one({'_id': session_id})
            if session:
                self.sessions[session_id] = session
        if session_id in self.sessions[session_id]:
                return self.sessions[session_id]['person_id']
        return False
    
    def hash_password(self, username, password):
        m = hashlib.md5()
        m.update(username + ":" + password + ":" + self.salt)
        pwhash = m.digest()
        return pwhash

    def verify_password(self, username, password):
        # Returns session id if match
        person = self.db.people.fetch_one({'name': username})
        if not person:
            raise ValueError("User not found")
        pwhash = self.hash_password(username, password)
        if pwhash != person.password:
            raise ValueError("Password invalid")
        # User authenticated
        session_id = self.db.sessions.insert_one({'person_id': person._id}).inserted_id
        session_id = str(session_id)
        return session_id
    
    def create_user(self, username, password):
        # Creates or takes ownership of an unclaimed user profile
        person = self.db.people.fetch_one({'name': username})
        pwhash = self.hash_password(username, password)
        if person:
            # Take ownership of unclaimed acct
            person_id = person._id
            if person.password:
                raise ValueError("User account already created!")
            updated_data = {
                'password': pwhash,
                'roles': self.new_user_roles
            }
            self.db.people.update_one({'_id': person._id}, updated_data)
        else:
            # Create new acct
            inserted_data = {
                'name': username,
                'password': pwhash,
                'roles': self.new_user_roles
            }
            inserted_data.update(self.default_player_profile)
            person_id = self.db.people.insert_one(inserted_data).inserted_id
        person_id = str(person_id)
        return person_id